// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAgPXemBCVEVtKDZZHGW6G2VEthBXx-8yQ",
  authDomain: "netflix-react-70f52.firebaseapp.com",
  projectId: "netflix-react-70f52",
  storageBucket: "netflix-react-70f52.appspot.com",
  messagingSenderId: "674371886205",
  appId: "1:674371886205:web:cf14c8494f09a08134a514",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);