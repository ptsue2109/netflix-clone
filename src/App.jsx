import Navbar from "./components/Navbar";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import { AuthContextProvider } from "./context/AuthContext";
import Login from "./pages/Login";
import SignUpPage from './pages/SignUp';
import Account from './pages/Account';
import Detail from "./pages/Detail"
function App() {
  return (
    <AuthContextProvider>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUpPage />} />
        <Route path="/account" element={<Account />} />
        <Route path="/movie/:id" element={<Detail />} />
      </Routes>
    </AuthContextProvider>
  );
}

export default App;
