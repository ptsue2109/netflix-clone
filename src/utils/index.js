export const truncateString = (str, num) =>{
    if(str?.length > num){
        return str.slice(0,num) + '...';
    }else{
        return str;
    }
}


export const formatRunTimeToDate = (num) => {
    let hours = Math.floor(num / 60);  
    let minutes = num % 60;
    if (minutes + ''.length < 2) {
      minutes = '0' + minutes; 
    }
    return hours + "h" + minutes+"m";
    
}

export const currency = (data) => {
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
      });
    const res = (formatter.format(data)).slice(0,-3)
    return res
}