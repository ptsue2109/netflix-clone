import { Modal } from 'antd';
import { useState } from 'react'
import { FaHeart, FaRegHeart } from "react-icons/fa";
import axios from 'axios';
import { Link } from 'react-router-dom';
const Movie = ({ item }) => {
    const [like, setLike] = useState(false);
    const [detail, setDetail] = useState({});
    const [isModalOpen, setIsModalOpen] = useState(false);
    const showModal = (id) => {
        setIsModalOpen(true);
        axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=9bd1fca078c46b654329af4ca6eef6b3`).then(({ data }) => {
            setDetail(data);
        })
    };
    const handleOk = () => { setIsModalOpen(false) };
    const handleCancel = () => { setIsModalOpen(false) };
    return (
        <div className="w-[180px] bg-black sm:w-[200px] md:w-[240px] lg:w-[280px] inline-block cursor-pointer relative p-2 ">
            <img className='w-full h-auto block' src={`https://image.tmdb.org/t/p/w500/${item?.backdrop_path}`} alt={item?.title} />
            <div className="absolute w-full top-0 left-0 h-full hover:bg-black/80  opacity-0 hover:opacity-100 text-white">
                <p onClick={() => showModal(item?.id)} className='white-space-normal text-xs md:text-sm font-bold flex justify-center items-center h-full text-center'>{item?.title}</p>
                <Modal title={
                    <span className="font-extrabold text-2xl  bg-clip-text text-transparent bg-gradient-to-r from-red-300 to-red-500">
                        {item?.title}
                    </span>}
                    open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer={null}>
                    <i> <b>Description: </b> {detail?.overview}</i> <br />
                    <i> <b>Genres: </b>{detail?.genres && detail?.genres?.map((item) => <span key={item.id}>{item?.name.concat(" ")}</span>)}</i> <br />
                    <i> <b>Release date: </b> {detail?.release_date}</i> <br />
                    <p className='mt-3'>  <Link to={`/movie/${item?.id}`} state={detail} className='bg-red-600 text-white px-2 py-2 rounded-r-sm'>Watch</Link> </p>
                </Modal>
                <p className=''>
                    {like ? <FaHeart className='absolute top-4 left-4 text-gray-300' /> : <FaRegHeart className='absolute top-4 left-4 text-gray-300' />}
                </p>
            </div>
        </div>
    )
}

export default Movie