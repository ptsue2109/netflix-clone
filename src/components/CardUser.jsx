import React from 'react'
import { Link } from 'react-router-dom'
import { MdChevronRight, MdChevronLeft } from 'react-icons/md';
import {truncateString} from "../utils";

const CardUser = ({ data, width }) => {
    const slideLeft = () => {
        var slider = document.getElementById('slider');
        slider.scrollLeft = slider.scrollLeft - 400;
    };
    const slideRight = () => {
        var slider = document.getElementById('slider');
        slider.scrollLeft = slider.scrollLeft + 400;
    };
    return (
        <>
            <div className="relative flex items-center group gap-3">
                <MdChevronLeft
                    onClick={slideLeft}
                    size={40}
                    className='bg-white left-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10  hidden group-hover:block'
                />

                <div id={'slider'} className="w-full h-full overflow-x-scroll whitespace-nowrap scroll-smooth scrollbar-hide relative">
                    {data && data.slice(0, 14).map((item, index) => (
                        <div key={index} className=" inline-block cursor-pointer relative pt-3 pl-3">
                            <div className="border w-[150px] h-[250px] rounded">
                                <img className='w-[150px] h-[175px] rounded-t-lg select-none object-cover block' src={`https://image.tmdb.org/t/p/w500/${item?.profile_path}`} />
                                <Link to="#" className='px-2 pt-5 font-bold truncate  hover:text-gray-400'>{truncateString(item?.name,14)}</Link>
                                <p className='px-2 text-black truncate text-ellipsis cursor-default'>
                                    {item?.character}
                                </p>
                            </div>
                        </div>
                    ))}
                <Link to='#' className='w-[150px] h-[250px] relative top-[-7em]  hover:text-gray-400' >Read more...</Link>
                </div>
                
                <MdChevronRight
                    onClick={slideRight}
                    size={40}
                    className='bg-white right-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block'
                />
            </div>
        </>
    )
}

export default CardUser
