import { Suspense, useState } from 'react';
import { Spin, Tabs, Card } from 'antd';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { truncateString } from '../utils';
import moment from 'moment';

/** NOTE : 
 * type = 1 =>  discussions
 * type = 2 =>  media
*/
const MovieTab = ({ data, type }) => {
    const [discussions, setDiscussions] = useState([]);
    const [media, setMedia] = useState([]);
    const items = [
        {
            key: '0',
            label: <b className='font-bold text-[22.4px] opacity-[9999999]'>Social</b>,

        },
        {
            key: '1',
            label: <><b>Reviews</b><span className='text-gray-600 pl-2'> (0) </span></>,
            children: `We don't have any reviews for ${data?.title}`,
        },
        {
            key: '3',
            label: <><b> Discussions</b><span className='text-gray-600 pl-2'>({discussions ? discussions.length : 0})</span></>,
            children: <Suspense fallback={<Spin />}>
                {discussions && discussions?.map((item, index) => (
                    <div className='flex items-center rounded h-[50px] w-[905px] border shadow-lg gap-3 mb-3 p-3' key={`${item.id}.${index}`}>
                        <img src={'https://www.gravatar.com/avatar/yHGV91jVzmqpFOtRSHF0avBZmPm.jpg'} className=' flex-none w-[25px] h-[25px] rounded-full' alt={item?.author} />
                        <Link to={"#"} className='w-[700px] truncate flex-auto w-83'> {truncateString(item?.content, 100)} </Link>
                        <div className="flex-auto w-16 ">
                            <p className='text-[10px]'>{moment(item?.created_at).format('LL')}</p>
                            <p>by <Link to={"#"} className='font-semibold'>{item?.author}</Link></p>
                        </div>
                    </div>
                ))}
            </Suspense>,
        },
    ];
    const itemsmedia = [
        {
            key: '0',
            label: <b className='font-bold text-[22.4px] opacity-[9999999]'>Media</b>,
        },
        {
            key: '1',
            label: <><b> Most Popular </b>   </>,
            children: <>
                <div className=' max-h-[100px] w-full flex'>
                    <img className='h-[300px] w-[533px] object-cover object-left-top'
                        src={`https://image.tmdb.org/t/p/w500/${data?.backdrop_path}`}
                        alt='/' />
                    <img className='h-[300px] w-[200px] object-cover'
                        src={`https://image.tmdb.org/t/p/w500/${data?.poster_path}`}
                        alt='/' />
                </div>
            </>,

        },
        {
            key: '2',
            label: <><b> Videos </b><span className='text-gray-600 pl-2'> 0 </span></>,
            children: `We don't have any reviews for ${data?.title}`,
        },
        {
            key: '3 ',
            label: <><b>  Backdrops </b><span className='text-gray-600 pl-2'> 0 </span></>,
            children: `We don't have any reviews for ${data?.title}`,
        },
        {
            key: '4',
            label: <><b>  Posters</b><span className='text-gray-600 pl-2'>({discussions ? discussions.length : 0})</span></>,
            children: <Suspense fallback={<Spin />}>
                {discussions && discussions?.map((item, index) => (
                    <div className='flex items-center rounded h-[50px] w-[905px] border shadow-lg gap-3 mb-3 p-3' key={`${item.id}.${index}`}>
                        <img src={'https://www.gravatar.com/avatar/yHGV91jVzmqpFOtRSHF0avBZmPm.jpg'} className=' flex-none w-[25px] h-[25px] rounded-full' alt={item?.author} />
                        <Link to={"#"} className='w-[700px] truncate flex-auto w-83'> {truncateString(item?.content, 100)} </Link>
                        <div className="flex-auto w-16 ">
                            <p className='text-[10px]'>{moment(item?.created_at).format('LL')}</p>
                            <p>by <Link to={"#"} className='font-semibold'>{item?.author}</Link></p>
                        </div>
                    </div>
                ))}
            </Suspense>,
        },
    ];
    console.log(discussions)
    const handleChange = (key) => {
        if (key === "3") {
            axios.get(`https://api.themoviedb.org/3/movie/${data?.id}/reviews?api_key=9bd1fca078c46b654329af4ca6eef6b3&language=en-US&page=1`).then(({ data }) => {
                setDiscussions(data.results)
            })
        }
    }
    return (
        <div className='flex gap-4  items-center h-full max-h-[100vh] max-h-screen '>
            {/* <p className='font-bold text-[22.4px] opacity-[9999999]'>Social</p> */}
            {type == 1 && <Tabs defaultActiveKey="1" size={'large'} items={items} onChange={handleChange} tabBarStyle={{ marginTop: "5px", color: "#000" }} tabBarGutter={50} />}
            {type == 2 && <Tabs defaultActiveKey="1" size={'large'} items={itemsmedia} onChange={handleChange} tabBarStyle={{ marginTop: "5px", color: "#000" }} tabBarGutter={50} />}

        </div>
    )
}

export default MovieTab
