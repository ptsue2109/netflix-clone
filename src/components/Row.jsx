import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Movie from './Movie';
import { MdChevronRight, MdChevronLeft } from 'react-icons/md';

const Row = ({ title, fetchURL, rowID }) => {
    const [movies, setMovies] = useState([]);

    useEffect(() => {
        axios.get(fetchURL).then(({ data }) => {
            setMovies(data.results)
        })
    }, [fetchURL]);

    const slideLeft = () => {
        var slider = document.getElementById('slider' + rowID);
        slider.scrollLeft = slider.scrollLeft - 400;
      };
      const slideRight = () => {
        var slider = document.getElementById('slider' + rowID);
        slider.scrollLeft = slider.scrollLeft + 400;
      };
    return (
        <>
            <h2 className='text-white font-bold md:text-xl p-4 bg-black'>{title}</h2>
            <div className="relative flex items-center group bg-black">
                <MdChevronLeft
                    onClick={slideLeft}
                    size={40}
                    className='bg-white left-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10  hidden group-hover:block'
                />

                <div id={'slider'+ rowID} className="w-full h-full overflow-x-scroll whitespace-nowrap scroll-smooth scrollbar-hide relative">
                    {movies.map((item, index) => (
                        <Movie key={index} item={item} />
                    ))}
                </div>
                <MdChevronRight
                    onClick={slideRight}
                    size={40}
                    className='bg-white right-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block'
                />
            </div>
        </>
    )
}

export default Row