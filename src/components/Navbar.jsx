import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <div className='w-full absolute flex justify-between items-center p-4 z-[100]'>
      <Link to="/"><img className='w-[125px] max-w-[125px] p-4' src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg" alt="" /></Link>
      <div>
        <Link to="/login"><button className='text-white pr-4'>Sign In</button></Link>
        <Link to="/signup"> <button className='bg-red-600 rounded px-4 py-1 cursor-pointer text-white'>Sign Up</button></Link>
      </div>
    </div>
  )
}

export default Navbar