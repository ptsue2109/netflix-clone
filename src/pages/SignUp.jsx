import {useState} from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { UserAuth } from '../context/AuthContext' ;
import { Input, Button} from "antd";

const SignUpPage = () => {
  const {user, signUp} = UserAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate()
  const handleSubmit = async(event) => {
    event.preventDefault();
    try {
      await signUp(email, password);
      navigate('/')
    } catch (error) {
      console.log(error);
    }

  }
  return (
    <div className='w-full h-screen '>
      <img className='hidden sm:block absolute w-full h-full object-cover'
        src='https://assets.nflxext.com/ffe/siteui/vlv3/f841d4c7-10e1-40af-bcae-07a3f8dc141a/f6d7434e-d6de-4185-a6d4-c77a2d08737b/US-en-20220502-popsignuptwoweeks-perspective_alpha_website_medium.jpg'
        alt='/'
      />
      <div className="bg-black/60 fixed top-0 left-0 w-full h-screen"></div>
      <div className="fixed w-full px-4 py-24 z-50">
        <div className='max-w-[450px] h-[600px] mx-auto bg-black/75 text-white'>
          <div className="max-w-[320px] mx-auto py-16">
            <h1 className='text-3xl font-bold'>Sign Up</h1>
            <form onSubmit={handleSubmit} className="w-full flex flex-col py-4" >
              <input type="email"
                className='p-3 my-2 bg-gray-700 rounded outline-none'
                name="email"
                id="email"
                placeholder='Email'
                autoComplete='email'
                onChange={(e) => setEmail(e.target.value)}
              />
              <input type="password"
                className='p-3 my-2 bg-gray-700 rounded outline-none'
                name="password"
                id="password"
                placeholder='Password'
                autoComplete='current-password'
                onChange={(e) => setPassword(e.target.value)}
              />
              <button className='bg-red-600 py-3 my-6 rounded font-bold'>SignUp</button>
              <div className="flex justify-between items-center text-sm text-gray-600">
                <p><input type="checkbox" /> Remember me</p>
                <p>Need Help?</p>
              </div>
              <div className='py-8 flex justify-between items-center'>
                <span className='text-gray-600s'>Already subscribed to Netflix?</span>
                <Link to="/login" className='text-red-600 pl-12 hover:text-gray-600'>Sign In</Link>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
  )
}

export default SignUpPage