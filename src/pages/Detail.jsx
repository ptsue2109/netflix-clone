import { useState, useEffect } from 'react'
import { Link, useLocation, useParams } from 'react-router-dom';
import { formatRunTimeToDate, currency } from '../utils';
import UserScoreChart from './../components/UserScoreChart';
import { AiOutlineHeart } from "react-icons/ai"
import { BsFillPlayFill } from "react-icons/bs"
import { FaFacebook } from "react-icons/fa"
import axios from 'axios';
import CardUser from '../components/CardUser';
import MovieTab from '../components/MovieTab';

const Detail = () => {
    const [detail, setDetail] = useState({});
    const [cast, setCast] = useState([]);
    const [crew, setCrew] = useState([]);
    const [kw, setKw] = useState([]);
    const { id } = useParams()
    let { state } = useLocation();
    useEffect(() => {
        if (state !== null) {
            setDetail(state);
            axios.get(`https://api.themoviedb.org/3/movie/${state?.id}/credits?api_key=9bd1fca078c46b654329af4ca6eef6b3&language=en-US`).then(({ data }) => {
                setCast(data?.cast);
                setCrew(data?.crew);
            })
        } else {
            axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=9bd1fca078c46b654329af4ca6eef6b3`).then(({ data }) => {
                setDetail(data);
            })
            axios.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=9bd1fca078c46b654329af4ca6eef6b3&language=en-US`).then(({ data }) => {
                setCast(data?.cast);
                setCrew(data?.crew);
            })
        }
        axios.get(`https://api.themoviedb.org/3/movie/${id}/keywords?api_key=9bd1fca078c46b654329af4ca6eef6b3`).then(({ data }) => {
            setKw(data?.keywords);
        })
    }, [state, id]);
    return (
        <div className='w-full h-full'>
            <div className="header h-[600px] border border-blue-600 mb-8">
                <div className=" h-[600px] w-full">
                    <img className='hidden sm:block w-full h-[600px] object-cover '
                        src={`https://image.tmdb.org/t/p/w500/${detail?.backdrop_path}`}
                        alt='/'
                    />
                    <div className="absolute bg-black/80 top-0 left-0 w-full h-[600px]">
                        <div className="h-full top-[15%] px-8 w-full z-50">
                            <div className='h-[450px] absolute top-[20%]  rounded-lg flex flex-row gap-7'>
                                <img className=' w-full h-full object-cover rounded-lg basis-[20%]'
                                    src={`https://image.tmdb.org/t/p/w500/${detail?.poster_path}`}
                                    alt='/'
                                />
                                <div className="content basis-[80%] p-3">
                                    <div className="title">
                                        <span className='font-bold text-[32px] text-white'>{detail?.title}</span>
                                        <span className='text-[32px] text-gray-600'> ({detail?.release_date?.slice(0, 4)})</span>
                                    </div>
                                    <div className="facts">
                                        <span className='border rounded-sm mr-2 px-1 text-gray-400'>TV-MA</span>
                                        <span className='text-white mr-2'>{detail?.release_date}({detail?.production_companies?.[0]?.origin_country})</span>
                                        <span className="text-white mr-2 before:contents-[*] before:text-red-600 after:tetx-red-600 before:mr-2 after:contents-[*] after:mr-2 ">
                                            {detail?.genres?.map((item) => <Link to="#" className='p-1' key={item.id}>{item?.name.concat(",")}</Link>)}
                                        </span>
                                        <span className='text-white'>{formatRunTimeToDate(140)}</span>
                                    </div>

                                    <div className="flex gap-3  items-center mb-10">
                                        <UserScoreChart per={(Math.floor(detail?.vote_average)) * 10} />
                                        <div className="flex  gap-4 pl-[150px] pt-[20px] ">
                                            <button className='bg-[#032541] w-[50px] h-[50px] rounded-full  flex justify-center items-center '><AiOutlineHeart style={{ fontSize: "20px", color: '#ffff' }} /></button>
                                            <button className='bg-[#032541] w-[50px] h-[50px] rounded-full flex justify-center items-center '><AiOutlineHeart style={{ fontSize: "20px", color: '#ffff' }} /></button>
                                            <button className='bg-[#032541] w-[50px] h-[50px] rounded-full flex justify-center items-center '><AiOutlineHeart style={{ fontSize: "20px", color: '#ffff' }} /></button>
                                            <button className='bg-[#032541] w-[50px] h-[50px] rounded-full flex justify-center items-center '><AiOutlineHeart style={{ fontSize: "20px", color: '#ffff' }} /></button>
                                            <Link to="" className='flex text-white font-bold pl-2 gap-1 items-center '><BsFillPlayFill /> Play Trailer</Link>
                                        </div>
                                    </div>

                                    <div className="tagline mb-3">
                                        <i className='text-gray-400 font-bold'>{detail?.tagline}</i>
                                    </div>
                                    <div className="overview mb-3">
                                        <b className='mb-3 text-[1.3em] text-white'>Overview</b>
                                        <p className='text-white text-[16px]'>{detail?.overview}</p>
                                    </div>
                                    <div className="castCrew grid grid-rows-2 grid-flow-col gap-4">
                                        {cast && cast.slice(0, 3)?.map((item, index) => (
                                            <div key={`${item?.id}.${index}`} >
                                                <div className='text-white text-[16px] font-semibold'> {item?.original_name}</div>
                                                <small className='text-white '>{item?.known_for_department === "Acting" ? 'Characters' : ""}</small>
                                            </div>

                                        ))}
                                        {crew && crew.slice(0, 3)?.map((item, index) => (
                                            <div key={`${item?.id}.${index}`}>
                                                <div className='text-white text-[16px] font-semibold'> {item?.original_name}</div>
                                                <small className='text-white '>{item?.known_for_department}</small>
                                            </div>

                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-full h-full h-screen min-h-[150vh] grid grid-cols-3 gap-4 px-4">
                <div className="content-left col-span-2">
                    <h1 className='font-semibold text-[22.4px]'>Top Billed Cast</h1>
                    <CardUser data={cast} />
                    <div className="mt-5 font-semibold hover:text-gray-400"><Link to="#">Full Cast & Crew</Link></div>
                    <hr className="my-10" />
                    <div className="">
                        <MovieTab data={detail} type={1} />
                        <div className="mt-5 font-semibold hover:text-gray-400"><Link to="#">Full Discussions</Link></div>

                    </div>
                    <hr className="my-10" />
                    <div className=""> <MovieTab data={detail} type={2} /></div>
                    <hr className="my-[300px]" />

                </div>
                <div className="content-right">
                    <div className="socials flex items-center gap-4 pt-3 pr-2">
                        <Link to="#" className=''><FaFacebook style={{ fontSize: "28px" }} /></Link>
                        <Link to="#" className=''><FaFacebook style={{ fontSize: "28px" }} /></Link>
                        <Link to="#" className='before:contents[" | "] before:text-red-600'><FaFacebook style={{ fontSize: "28px" }} /></Link>
                        <Link to="#" className=''><FaFacebook style={{ fontSize: "28px" }} /></Link>
                    </div>
                    <div className="facts">
                        <h1 className='mb-3 mt-3 font-bold'>Facts</h1>
                        <div className="mb-3">
                            <h1 className='font-bold'>Original Name</h1>
                            <p>{detail?.title}</p>
                        </div>
                        <div className="mb-3">
                            <h1 className='font-bold'>Status</h1>
                            <p>{detail?.status}</p>
                        </div>

                        {detail?.budget && detail?.revenue && (
                            <>
                                <div className="mb-3">
                                    <h1 className='font-bold'>Original Language</h1>
                                    <p>{detail?.spoken_languages?.[0]?.name}</p>
                                </div>
                                <div className="mb-3">
                                    <h1 className='font-bold'>Budget</h1>
                                    <p>{currency(detail?.budget)}</p>
                                </div>
                                <div className="mb-3">
                                    <h1 className='font-bold'>Revenue</h1>
                                    <p>{currency(detail?.revenue)}</p>
                                </div>
                            </>
                        )}
                        <div className="mb-3">
                            <h1 className='font-bold'>Keywords</h1>
                            {kw ? kw.map((item, index) => (
                                <div className="mb-2 flex flex-wrap w-[400px]" key={`${item.id}.${index}`}>
                                    <p className='bg-gray-200 p-1 text-black rounded' >{item?.name}</p>
                                </div>
                            )) : "Don't have any keywords"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Detail
